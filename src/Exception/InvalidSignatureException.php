<?php
declare(strict_types = 1);

namespace UwKluis\WebhookClient\Exception;

use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Throwable;

final class InvalidSignatureException extends RuntimeException
{
    /** @var ServerRequestInterface */
    private $request;

    /**
     * InvalidSignatureException constructor.
     *
     * @param string                 $message
     * @param int                    $code
     * @param Throwable|null         $previous
     * @param ServerRequestInterface $request
     */
    public function __construct(
        string $message = "",
        int $code = 0,
        Throwable $previous = null,
        ServerRequestInterface $request = null
    ) {
        parent::__construct($message, $code, $previous);
        $this->request = $request;
    }

    /**
     * @return ServerRequestInterface|null
     */
    public function getRequest()
    {
        return $this->request;
    }
}
